package main.java.hello;

import org.springframework.web.bind.annotation.RestController;

//import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

import java.util.Map;

import org.bson.Document;

//import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoClient;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class HelloController {
	
	 @RequestMapping("/")
	    public String index() {
		 MongoDatabase database = createConnection("springboot");
		
		      return "Greetings from Spring Boot!";
	    }
	 
	 @RequestMapping(value = "/itemcreate",method = RequestMethod.POST)
	 public void createItem(@RequestBody Map<String,Object> item){
		 String itemInfo = (String) item.get("itemname");
		 int costint = (int) item.get("cost");
		 
		 MongoDatabase database = createConnection("springboot");
		 MongoCollection<Document> mongoCollection= database.getCollection("shoppinglist");
		 Document doc = new Document("itemName",itemInfo).append("cost", costint);
		 mongoCollection.insertOne(doc);
		 
	 }
	 @RequestMapping("/hello")
	    public String indexhello(@RequestParam String name) {
	        return "Hello"+ name;
	    }
	 
	 @RequestMapping("/add")
	    public int addnums(@RequestParam int num1, @RequestParam int num2) {
	        return num1+num2;
	    }

	 public MongoDatabase createConnection(String databaseName){
		 MongoClient mongoClient = MongoClients.create("mongodb+srv://srilatha3:vignesh3@cluster0-lpcyt.mongodb.net/"+databaseName+"?retryWrites=true");
 		 MongoDatabase database = mongoClient.getDatabase(databaseName);
 		 return database;
		 
	 }
}